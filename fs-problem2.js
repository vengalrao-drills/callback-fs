const path = require("path");
const fs = require("fs");


const doOperations = (callback) => {
  pathfile = path.join(__dirname, "lipsum.txt");
  fs.readFile(pathfile, "utf-8", (error, data) => {
    // checking the file. if error calling the callback
    if (error) {
      callback(
        "function (readWordFiles) Error occured in creating the file ",
        error
      );
    } else {
      console.log(" question 1 -  Read the given file lipsum.txt\n");
      callback(null, data);
      console.log("\n- - - - Data fetched - - - - question 1 done above.\n");

      // 2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
      // now question 2
      const newFileUpperCase = (data) => {
        const newFilePath = path.join(__dirname, "filesnames.txt");
        content = data.toUpperCase(); // converting to uppercase
        fs.writeFile(newFilePath, content, "utf-8", (error, data) => {
          if (error) {
            // checking the file. if error calling the callback
            callback(" newFileUpperCase() error occured in creating file", err);
          } else {
            callback(
              null,
              `question 2 - content to uppercase & write to a new file\n${content}\n\- - - - Data converted to uppercase - - - -question 2 Done above.\n`
            );

            // 3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
            const newFileLowerCase = (newFilePath, content) => {
              fs.readFile(newFilePath, "utf-8", (err, data) => {
                // reading the file
                if (err) {
                  // checking the file. if error calling the callback
                  callback(err);
                } else {
                  content = data;
                  fs.writeFile(newFilePath, content, "utf-8", (err, path) => {
                    // writing the file
                    if (err) {
                      callback(
                        " newFileLowerCase() error occured in creating file",
                        err
                      );
                    } else {
                      callback(
                        null,
                        `question3 - convert it to lower case.\n${content.toLowerCase()}\n- - - - Data converted to lowercase - - - -question 3 Done above.\n`
                      );

                      // 4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
                      const readNewFile = (newFilePath) => {
                        fs.readFile(newFilePath, "utf-8", (error, data) => {
                          if (err) {
                            // checking the file. if error calling the callback
                            callback(
                              `readNewFile() error occured in creating file ${error}`
                            );
                          } else {
                            let words = data.split("\n");
                            words = words.map((word) => {
                              // will iterate and sort() it
                              word = word.split(" ");
                              word = word.sort().join(" ");
                              return word;
                            });
                            words = words.join("\n");
                            // console.log(words)
                            fs.writeFile(
                              newFilePath,
                              words,
                              "utf-8",
                              (err, data) => {
                                if (err) {
                                  // checking the file. if error calling the callback
                                  callback(err);
                                } else {
                                  callback(
                                    null,
                                    `question4 - sort the content, write it out to a new file\n${words}\n- - - - Data sorted - - - - question 4 done above.\n`
                                  );

                                  // 5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
                                  const deleteFiles = (newFilePath) => {
                                    fs.unlink(newFilePath, (err, data) => { // unlink is used to delete files
                                      if (err) {
                                        // checking the file. if error calling the callback
                                        callback(
                                          `function(deleteFiles) - Unfortunatley, error occured ${err}`
                                        );
                                      } else {
                                        callback(
                                          null,
                                          "question5\n- - - - Successfully file removed - - - - question 5 done\n"
                                        );
                                      }
                                    });
                                  };
                                  deleteFiles(newFilePath);
                                }
                              }
                            );
                          }
                        });
                      };
                      readNewFile(newFilePath);
                    }
                  });
                }
              });
            };
            newFileLowerCase(newFilePath, content);
          }
        });
      };
      newFileUpperCase(data);
    }
  });
};

module.exports = doOperations;
// doOperations((err, data)=>{
//   if(err){
//     console.log(err)
//   }else{
//     console.log(data)
//   }
// })








//output
// question 1 -  Read the given file lipsum.txt

// Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam quis elit nisl. Aliquam congue ut orci quis gravida. Etiam eleifend ultrices turpis, ut gravida ligula accumsan vel. Cras sagittis ex ac rutrum congue. Curabitur tristique nibh posuere nisi varius, non ullamcorper sapien venenatis. Maecenas non suscipit ex. Etiam commodo magna ullamcorper ex dignissim, sit amet lacinia dolor molestie. Morbi vitae scelerisque magna. Phasellus pulvinar, mauris id ornare consequat, elit sem fermentum nisl, eget varius sapien risus at felis.

// Curabitur eu accumsan turpis. Fusce finibus sem euismod massa fringilla, at maximus sem pulvinar. Proin sollicitudin porta leo sed pellentesque. Mauris nisi dolor, posuere in neque sit amet, sodales lobortis justo. Etiam venenatis metus aliquet purus porta, a aliquam nunc ullamcorper. Maecenas dapibus nibh non nunc facilisis, sit amet consequat lectus scelerisque. Pellentesque neque elit, sodales a commodo vel, vulputate in diam. Quisque quis lacus hendrerit, viverra dolor vel, volutpat nisi. Sed facilisis sollicitudin lorem, sit amet vulputate ipsum convallis sit amet. Praesent ipsum orci, vestibulum non consectetur id, molestie sit amet sem. Phasellus interdum purus id erat tempor rhoncus. In viverra quam et neque placerat malesuada. Donec interdum nulla non ante finibus, eget posuere sem ultricies.

// Vestibulum tempor aliquam ipsum sit amet pretium. Quisque egestas risus nisi, vel pharetra arcu ultricies a. Quisque ornare nunc sit amet libero pharetra consectetur. Aenean ligula dui, faucibus vitae tempor in, molestie ut nibh. Donec finibus ex ex, et aliquet urna accumsan et. Duis felis nisi, tincidunt nec magna sed, tincidunt volutpat lectus. Mauris at gravida ipsum. Duis vel velit lectus. Etiam auctor fermentum tortor, eget condimentum elit.

// - - - - Data fetched - - - - question 1 done above.

// question 2 - content to uppercase & write to a new file
// LOREM IPSUM DOLOR SIT AMET, CONSECTETUR ADIPISCING ELIT. ALIQUAM QUIS ELIT NISL. ALIQUAM CONGUE UT ORCI QUIS GRAVIDA. ETIAM ELEIFEND ULTRICES TURPIS, UT GRAVIDA LIGULA ACCUMSAN VEL. CRAS SAGITTIS EX AC RUTRUM CONGUE. CURABITUR TRISTIQUE NIBH POSUERE NISI VARIUS, NON ULLAMCORPER SAPIEN VENENATIS. MAECENAS NON SUSCIPIT EX. ETIAM COMMODO MAGNA ULLAMCORPER EX DIGNISSIM, SIT AMET LACINIA DOLOR MOLESTIE. MORBI VITAE SCELERISQUE MAGNA. PHASELLUS PULVINAR, MAURIS ID ORNARE CONSEQUAT, ELIT SEM FERMENTUM NISL, EGET VARIUS SAPIEN RISUS AT FELIS.

// CURABITUR EU ACCUMSAN TURPIS. FUSCE FINIBUS SEM EUISMOD MASSA FRINGILLA, AT MAXIMUS SEM PULVINAR. PROIN SOLLICITUDIN PORTA LEO SED PELLENTESQUE. MAURIS NISI DOLOR, POSUERE IN NEQUE SIT AMET, SODALES LOBORTIS JUSTO. ETIAM VENENATIS METUS ALIQUET PURUS PORTA, A ALIQUAM NUNC ULLAMCORPER. MAECENAS DAPIBUS NIBH NON NUNC FACILISIS, SIT AMET CONSEQUAT LECTUS SCELERISQUE. PELLENTESQUE NEQUE ELIT, SODALES A COMMODO VEL, VULPUTATE IN DIAM. QUISQUE QUIS LACUS HENDRERIT, VIVERRA DOLOR VEL, VOLUTPAT NISI. SED FACILISIS SOLLICITUDIN LOREM, SIT AMET VULPUTATE IPSUM CONVALLIS SIT AMET. PRAESENT IPSUM ORCI, VESTIBULUM NON CONSECTETUR ID, MOLESTIE SIT AMET SEM. PHASELLUS INTERDUM PURUS ID ERAT TEMPOR RHONCUS. IN VIVERRA QUAM ET NEQUE PLACERAT MALESUADA. DONEC INTERDUM NULLA NON ANTE FINIBUS, EGET POSUERE SEM ULTRICIES.

// VESTIBULUM TEMPOR ALIQUAM IPSUM SIT AMET PRETIUM. QUISQUE EGESTAS RISUS NISI, VEL PHARETRA ARCU ULTRICIES A. QUISQUE ORNARE NUNC SIT AMET LIBERO PHARETRA CONSECTETUR. AENEAN LIGULA DUI, FAUCIBUS VITAE TEMPOR IN, MOLESTIE UT NIBH. DONEC FINIBUS EX EX, ET ALIQUET URNA ACCUMSAN ET. DUIS FELIS NISI, TINCIDUNT NEC MAGNA SED, TINCIDUNT VOLUTPAT LECTUS. MAURIS AT GRAVIDA IPSUM. DUIS VEL VELIT LECTUS. ETIAM AUCTOR FERMENTUM TORTOR, EGET CONDIMENTUM ELIT.
// - - - - Data converted to uppercase - - - -question 2 Done above.

// question3 - convert it to lower case.
// lorem ipsum dolor sit amet, consectetur adipiscing elit. aliquam quis elit nisl. aliquam congue ut orci quis gravida. etiam eleifend ultrices turpis, ut gravida ligula accumsan vel. cras sagittis ex ac rutrum congue. curabitur tristique nibh posuere nisi varius, non ullamcorper sapien venenatis. maecenas non suscipit ex. etiam commodo magna ullamcorper ex dignissim, sit amet lacinia dolor molestie. morbi vitae scelerisque magna. phasellus pulvinar, mauris id ornare consequat, elit sem fermentum nisl, eget varius sapien risus at felis.

// curabitur eu accumsan turpis. fusce finibus sem euismod massa fringilla, at maximus sem pulvinar. proin sollicitudin porta leo sed pellentesque. mauris nisi dolor, posuere in neque sit amet, sodales lobortis justo. etiam venenatis metus aliquet purus porta, a aliquam nunc ullamcorper. maecenas dapibus nibh non nunc facilisis, sit amet consequat lectus scelerisque. pellentesque neque elit, sodales a commodo vel, vulputate in diam. quisque quis lacus hendrerit, viverra dolor vel, volutpat nisi. sed facilisis sollicitudin lorem, sit amet vulputate ipsum convallis sit amet. praesent ipsum orci, vestibulum non consectetur id, molestie sit amet sem. phasellus interdum purus id erat tempor rhoncus. in viverra quam et neque placerat malesuada. donec interdum nulla non ante finibus, eget posuere sem ultricies.

// vestibulum tempor aliquam ipsum sit amet pretium. quisque egestas risus nisi, vel pharetra arcu ultricies a. quisque ornare nunc sit amet libero pharetra consectetur. aenean ligula dui, faucibus vitae tempor in, molestie ut nibh. donec finibus ex ex, et aliquet urna accumsan et. duis felis nisi, tincidunt nec magna sed, tincidunt volutpat lectus. mauris at gravida ipsum. duis vel velit lectus. etiam auctor fermentum tortor, eget condimentum elit.
// - - - - Data converted to lowercase - - - -question 3 Done above.

// question4 - sort the content, write it out to a new file
// AC ACCUMSAN ADIPISCING ALIQUAM ALIQUAM AMET AMET, AT COMMODO CONGUE CONGUE. CONSECTETUR CONSEQUAT, CRAS CURABITUR DIGNISSIM, DOLOR DOLOR EGET ELEIFEND ELIT ELIT ELIT. ETIAM ETIAM EX EX EX. FELIS. FERMENTUM GRAVIDA GRAVIDA. ID IPSUM LACINIA LIGULA LOREM MAECENAS MAGNA MAGNA. MAURIS MOLESTIE. MORBI NIBH NISI NISL, NISL. NON NON ORCI ORNARE PHASELLUS POSUERE PULVINAR, QUIS QUIS RISUS RUTRUM SAGITTIS SAPIEN SAPIEN SCELERISQUE SEM SIT SIT SUSCIPIT TRISTIQUE TURPIS, ULLAMCORPER ULLAMCORPER ULTRICES UT UT VARIUS VARIUS, VEL. VENENATIS. VITAE

// A A ACCUMSAN ALIQUAM ALIQUET AMET AMET AMET AMET, AMET. ANTE AT COMMODO CONSECTETUR CONSEQUAT CONVALLIS CURABITUR DAPIBUS DIAM. DOLOR DOLOR, DONEC EGET ELIT, ERAT ET ETIAM EU EUISMOD FACILISIS FACILISIS, FINIBUS FINIBUS, FRINGILLA, FUSCE HENDRERIT, ID ID, IN IN IN INTERDUM INTERDUM IPSUM IPSUM JUSTO. LACUS LECTUS LEO LOBORTIS LOREM, MAECENAS MALESUADA. MASSA MAURIS MAXIMUS METUS MOLESTIE NEQUE NEQUE NEQUE NIBH NISI NISI. NON NON NON NULLA NUNC NUNC ORCI, PELLENTESQUE PELLENTESQUE. PHASELLUS PLACERAT PORTA PORTA, POSUERE POSUERE PRAESENT PROIN PULVINAR. PURUS PURUS QUAM QUIS QUISQUE RHONCUS. SCELERISQUE. SED SED SEM SEM SEM SEM. SIT SIT SIT SIT SIT SODALES SODALES SOLLICITUDIN SOLLICITUDIN TEMPOR TURPIS. ULLAMCORPER. ULTRICIES. VEL, VEL, VENENATIS VESTIBULUM VIVERRA VIVERRA VOLUTPAT VULPUTATE VULPUTATE

// A. ACCUMSAN AENEAN ALIQUAM ALIQUET AMET AMET ARCU AT AUCTOR CONDIMENTUM CONSECTETUR. DONEC DUI, DUIS DUIS EGESTAS EGET ELIT. ET ET. ETIAM EX EX, FAUCIBUS FELIS FERMENTUM FINIBUS GRAVIDA IN, IPSUM IPSUM. LECTUS. LECTUS. LIBERO LIGULA MAGNA MAURIS MOLESTIE NEC NIBH. NISI, NISI, NUNC ORNARE PHARETRA PHARETRA PRETIUM. QUISQUE QUISQUE RISUS SED, SIT SIT TEMPOR TEMPOR TINCIDUNT TINCIDUNT TORTOR, ULTRICIES URNA UT VEL VEL VELIT VESTIBULUM VITAE VOLUTPAT
// - - - - Data sorted - - - - question 4 done above.

// question5
// - - - - Successfully file removed - - - - question 5 done