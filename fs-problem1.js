const fs = require("fs");
const path = require("path");

const makeAndDelete = (callback) => {
  let newDirectory = "RandomFolder";
  fs.mkdir(newDirectory, { recursive: true }, (error) => {
    // new directory
    if (error) {
      callback("Unfortunately, Error happened in making a new Directory");
    } else {
      let filesCreated = Math.floor(Math.random() * 10) + 1;

      console.log(
        `- - - - Randomly ${filesCreated} are going to be created - - - - `
      );
      console.log("started");

      let createFile = (index) => {
        if (index <= filesCreated) {
          let filepath = path.join(
            newDirectory,
            // __dirname,
            //  newDirectory,
            `RandomFile${index}.json`
          ); // file path created
          console.log(filepath);
          let content = `this file name is RandomFile${index}`; // for every path filepath is different. recursively it is created
          fs.writeFile(filepath, content, "utf-8", (err) => {
            if (err) {
              callback(`error in writing the filepath ${err}`);
            } else {
              callback(null, `filename :-  RandomFile .json created`);
              createFile(index + 1);
            }
          });
        } else {
          console.log("- - - - files getting deleting now");
          let deleteFile = (delIndex) => {
            let filesCreatednow = filesCreated;
            if (delIndex <= filesCreatednow) {
              let filepath = path.join(
                __dirname,
                // `/${newDirectory}/`,
                `/test/${newDirectory}/RandomFile${delIndex}.json`
              );
              console.log(filepath);
              fs.unlink(filepath, (err) => {
                // fs.unlink is used to delete the file.
                if (err) {
                  console.log(
                    `Problem in deleting the file, RandomFile${delIndex}.json   ${err}`
                  );
                } else {
                  console.log(`file deleted RandomFile${delIndex}.json`);
                  deleteFile(delIndex + 1);
                }
              });
            } else {
              let filepath = path.join(`${newDirectory}/`);
              console.log(filepath);
              fs.rmdir(filepath, (err) => {
                // removing the directory - rmdir is useful to remove the directory!
                if (err) {
                  console.log("error in deleting the directory");
                } else {
                  console.log("Both files and directory is deleted");
                }
              });
            }
          };
          deleteFile(1);
        }
      };
      createFile(1);
    }
  });
};
module.exports = makeAndDelete;


//output


// - - - - Randomly 8 are going to be created - - - - 
// started
// RandomFolder/RandomFile1.json
// filename :-  RandomFile .json created
// RandomFolder/RandomFile2.json
// filename :-  RandomFile .json created
// RandomFolder/RandomFile3.json
// filename :-  RandomFile .json created
// RandomFolder/RandomFile4.json
// filename :-  RandomFile .json created
// RandomFolder/RandomFile5.json
// filename :-  RandomFile .json created
// RandomFolder/RandomFile6.json
// filename :-  RandomFile .json created
// RandomFolder/RandomFile7.json
// filename :-  RandomFile .json created
// RandomFolder/RandomFile8.json
// filename :-  RandomFile .json created
// - - - - files getting deleting now
// /home/patchava/work/callbackdrill2/test/RandomFolder/RandomFile1.json
// file deleted RandomFile1.json
// /home/patchava/work/callbackdrill2/test/RandomFolder/RandomFile2.json
// file deleted RandomFile2.json
// /home/patchava/work/callbackdrill2/test/RandomFolder/RandomFile3.json
// file deleted RandomFile3.json
// /home/patchava/work/callbackdrill2/test/RandomFolder/RandomFile4.json
// file deleted RandomFile4.json
// /home/patchava/work/callbackdrill2/test/RandomFolder/RandomFile5.json
// file deleted RandomFile5.json
// /home/patchava/work/callbackdrill2/test/RandomFolder/RandomFile6.json
// file deleted RandomFile6.json
// /home/patchava/work/callbackdrill2/test/RandomFolder/RandomFile7.json
// file deleted RandomFile7.json
// /home/patchava/work/callbackdrill2/test/RandomFolder/RandomFile8.json
// file deleted RandomFile8.json
// RandomFolder/
// Both files and directory is deleted